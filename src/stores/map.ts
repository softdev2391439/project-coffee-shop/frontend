import { ref, computed, type Ref } from 'vue'
import { defineStore } from 'pinia'

export const useMapStore = defineStore('map', () => {
  const useIn = ref('')
  const checkUser = ref(false)
  const lat = ref(0)
  const lng = ref(0)
  const branchAddress = ref<{ lat: number; lng: number; name: string } | null>(null)
  const map = ref()
  const popup = ref()
  const currentLat = ref(0)
  const currentLng = ref(0)
  const currentMarker: Ref<any | null> = ref(null)
  const currentLayer: Ref<any | null> = ref(null)
  const flageMarker: Ref<any | null> = ref(null)
  const brandMarker: Ref<any | null> = ref(null)
  return {
    lat,
    lng,
    branchAddress,
    map,
    popup,
    currentMarker,
    currentLayer,
    flageMarker,
    brandMarker,
    currentLat,
    currentLng,
    useIn,
    checkUser
  }
})
