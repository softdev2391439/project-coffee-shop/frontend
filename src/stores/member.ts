import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import memberService from '@/services/member'
import type { Member } from '@/types/Member'
import { useReceiptStore } from './receipt'

export const useMemberStore = defineStore('member', () => {
  const loadingStore = useLoadingStore()
  const members = ref<Member[]>([])
  const receiptStore = useReceiptStore()
  const initilMember: Member = {
    fristName: '',
    lastName: '',
    email: '',
    tel: '',
    birth: new Date(),
    point: 0,
    pointRate: 0,
    indate: new Date()
  }
  const editedMember = ref<Member>(JSON.parse(JSON.stringify(initilMember)))

  const currentMember = ref<Member | null>(null)
  async function searchMember(tel: string) {
    loadingStore.doLoad()
    const res = await memberService.getMembers()
    members.value = res.data
    loadingStore.finish()

    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
      clearFrom()
    } else {
      currentMember.value = members.value[index]
      receiptStore.receipt!.member = JSON.parse(JSON.stringify(currentMember.value))
    }
  }

  function usePoint(point: number) {
    members.value!.point = members.value!.point - point
  }

  function getCurrentMember(): Member | null {
    return currentMember.value
  }

  async function getMember(id: number) {
    loadingStore.doLoad()
    const res = await memberService.getMember(id)
    editedMember.value = res.data
    loadingStore.finish()
  }

  async function getMembers() {
    loadingStore.doLoad()
    const res = await memberService.getMembers()
    members.value = res.data
    loadingStore.finish()
  }

  async function saveMember() {
    loadingStore.doLoad()
    const member = editedMember.value
    if (!member.id) {
      const res = await memberService.addMember(member)
    } else {
      const res = await memberService.updateMember(member)
    }
    clearFrom()
    await getMembers()
    loadingStore.finish()
  }

  async function deleteMember() {
    loadingStore.doLoad()
    const member = editedMember.value
    const res = await memberService.delMember(member)
    clearFrom()
    await getMembers()
    loadingStore.finish()
  }

  function clearFrom() {
    editedMember.value = JSON.parse(JSON.stringify(initilMember))
  }

  function clear() {
    return {
      fristName: '',
      lastName: '',
      email: '',
      tel: '',
      birth: new Date(),
      point: 0,
      pointRate: 0,
      indate: new Date()
    }
  }

  async function updatePoints(memberId: number, newPoints: number) {
    loadingStore.doLoad();
    try {
      const response = await memberService.updatePoints(memberId, newPoints);
      const updatedMember = response.data;
      if (updatedMember) {
        const index = members.value.findIndex(member => member.id === memberId);
        if (index !== -1) {
          members.value[index].point = updatedMember.point;
        }
      } else {
        console.error('Failed to update member points: Member not found');
      }
    } catch (error) {
      console.error('Error updating member points:', error);
    }
    loadingStore.finish();
  }

  return {
    members,
    currentMember,
    getCurrentMember,
    searchMember,
    getMember,
    getMembers,
    saveMember,
    deleteMember,
    editedMember,
    clearFrom,
    clear,
    usePoint,
    updatePoints
  }
})
