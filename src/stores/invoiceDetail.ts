import { defineStore } from 'pinia'
import { nextTick, ref } from 'vue'
import { useLoadingStore } from '@/stores/loading'

import invoiceDetailService from '@/services/invoiceDetail'
import { useMaterialStore } from './material'
import type { Material } from '@/types/Material'
import type { InvoiceDetail } from '@/types/Invoice/InvoiceDetail'

export const useInvoiceDetailStore = defineStore('invoiceDetailStore', () => {
  const loadingStore = useLoadingStore()
  const materialStore = useMaterialStore()

  const dialogDelete = ref(false)
  const dialog = ref(false)
  const loading = ref(false)
  const invoiceId = ref<number>(-1) //invoice id
  const invoiceDetails = ref<InvoiceDetail[]>([])
  const addinvoiceDetails = ref<InvoiceDetail[]>([])
  const addAllinvoiceDetails = ref<InvoiceDetail[]>([])

  const initInvoiceDetail = ref<InvoiceDetail>({
    IND_ID: -1,
    IND_MATERIAL: -1,
    IND_AMOUT: 0,
    IND_PRICE: 0,
    IND_INVOICE: -1
  })
  const editedStockDetail = ref<InvoiceDetail>(JSON.parse(JSON.stringify(initInvoiceDetail.value)))


  async function getInvoiceDetails() {
    loadingStore.doLoad()
    try {
      const res = await invoiceDetailService.getInvoiceDetails()
      const invoiceDetailData = res.data.map((invoiceDetail: any) => {
        let invoiceID = -1
        if (invoiceDetail.invoice && invoiceDetail.invoice.INV_ID) {
          invoiceID = invoiceDetail.invoice.INV_ID
        }
        return {
          IND_ID: invoiceDetail.IND_ID,
          IND_MATERIAL: invoiceDetail.IND_MATERIAL.M_ID,
          IND_AMOUT: invoiceDetail.IND_AMOUT,
          IND_PRICE: invoiceDetail.IND_PRICE,
          IND_INVOICE: invoiceDetail.IND_INVOICE.INV_ID
        }
      })

      invoiceDetails.value = invoiceDetailData
      loadingStore.finish()
    } catch (e) {
      console.log('cant fetch data from database')
      loadingStore.finish()
    }
  }


  async function getInvoiceDetailsByInvoiceId(InvoiceId: number) {
    try {
      const res = await invoiceDetailService.getInvoiceDetailsByInvoiceId(InvoiceId)
      const invoiceDetailData = res.data.map((invoiceDetail: any) => {
        let invoiceID = -1
        if (invoiceDetail.invoice && invoiceDetail.invoice.INV_ID) {
          invoiceID = invoiceDetail.invoice.INV_ID
        }
        return {
          IND_ID: invoiceDetail.IND_ID,
          IND_MATERIAL: invoiceDetail.IND_MATERIAL.M_ID,
          IND_AMOUT: invoiceDetail.IND_AMOUT,
          IND_PRICE: invoiceDetail.IND_PRICE,
          IND_INVOICE: invoiceDetail.IND_INVOICE.INV_ID
        }
      })
      invoiceDetails.value = invoiceDetailData
      loadingStore.finish()
    } catch (e) {
      console.log('cant fetch data from database')
      loadingStore.finish()
    }
  }

  async function saveInvoiceDetail(invoiceDetail: InvoiceDetail) {
    loadingStore.doLoad()
    if (invoiceDetail.IND_ID < 0) {
      const res = await invoiceDetailService.addInvoiceDetail(invoiceDetail)
    } else {
      const res = await invoiceDetailService.updateInvoiceDetail(invoiceDetail)
    }
    await getInvoiceDetailsByInvoiceId(invoiceId.value)
    loadingStore.finish()
  }

  function editInvoiceDetail(invoiceDetail: InvoiceDetail) {
    editedStockDetail.value = Object.assign({}, invoiceDetail)
    dialog.value = true
  }

  function sendToDelete(invoiceDetail: InvoiceDetail) {
    editedStockDetail.value = invoiceDetail
    dialogDelete.value = true
  }

  async function deleteItemConfirm(invoiceDetail: InvoiceDetail) {
    await deleteInvoiceDetail(invoiceDetail)
    closeDelete()
  }

  async function deleteInvoiceDetail(invoiceDetail: InvoiceDetail) {
    loadingStore.doLoad()
    const res = await invoiceDetailService.delInvoiceDetail(invoiceDetail)
    await getInvoiceDetailsByInvoiceId(invoiceId.value)
    invoiceId.value = -1
    loadingStore.finish()
  }

  function closeDelete() {
    dialogDelete.value = false
    editedStockDetail.value = initInvoiceDetail.value
  }

  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      editedStockDetail.value = Object.assign({}, initInvoiceDetail.value)
    })
  }
  function getMaterialName(matrialId: number) {
    const material = materialStore.materials.find((mat: Material) => mat && mat.id === matrialId)
    return material ? material.name : 'Unknown Material'
  }
  function getMaterialType(matrialId: number) {
    const material = materialStore.materials.find((mat: Material) => mat && mat.id === matrialId)
    return material ? material.type : 'Unknown Material'
  }

  function getMinimum(materialId: number) {
    const material = materialStore.materials.find((mat: Material) => mat && mat.id === materialId)
    return material ? material.minimum : null
  }

  function getAmount(materialId: number) {
    const material = materialStore.materials.find((mat: Material) => mat && mat.id === materialId)
    return material ? material.amount : null
  }
  function getMat(materialId: number) {
    const material = materialStore.materials.find((mat: Material) => mat && mat.id === materialId)
    return material ? material : null
  }

  function addinvoiceDetail(mat: Material) {
    const newInvoiceDetail: InvoiceDetail = {
      IND_ID: -1,
      IND_AMOUT: mat.amount,
      IND_INVOICE: -1,
      IND_MATERIAL: mat.id,
      IND_PRICE: 0
    }
    addinvoiceDetails.value.push(newInvoiceDetail)
  }

  function updateInvoiceDetail(id: number, amount: number) {
    for (let i = 0; i < addinvoiceDetails.value.length; i++) {
      if (addinvoiceDetails.value[i].IND_MATERIAL == id) {
        addinvoiceDetails.value[i].IND_AMOUT = amount
      }
    }
  }

  function updateMatDetail() {
    for (let i = 0; i < addinvoiceDetails.value.length; i++) {
      for (let j = 0; j < addinvoiceDetails.value.length; j++) {
        if (addinvoiceDetails.value[i].IND_MATERIAL == materialStore.materials[j].id) {
          const updateMat: Material = {
            id: materialStore.materials[j].id,
            amount: addinvoiceDetails.value[i].IND_AMOUT,
            minimum: materialStore.materials[j].minimum,
            name: materialStore.materials[j].name,
            type: materialStore.materials[j].type
          }
          materialStore.saveMaterial(updateMat)
        }
      }
    }
  }

  function updateMatAllDetail() {
    for (let i = 0; i < addAllinvoiceDetails.value.length; i++) {
      for (let j = 0; j < materialStore.materials.length; j++) {
        if (addAllinvoiceDetails.value[i].IND_MATERIAL == materialStore.materials[j].id) {
          const updateMat: Material = {
            id: materialStore.materials[j].id,
            amount: addAllinvoiceDetails.value[i].IND_AMOUT,
            minimum: materialStore.materials[j].minimum,
            name: materialStore.materials[j].name,
            type: materialStore.materials[j].type
          }
          materialStore.saveMaterial(updateMat)
        }
      }
    }
  }

  function clearAddDetails() {
    addinvoiceDetails.value = []
  }

  function clearAddAllMat() {
    addAllinvoiceDetails.value = []
  }

  function addAllMat() {
    for (let i = 0; i < materialStore.materials.length; i++) {
      const newInvoiceDetail: InvoiceDetail = {
        IND_ID: -1,
        IND_AMOUT: materialStore.materials[i].amount,
        IND_INVOICE: -1,
        IND_MATERIAL: materialStore.materials[i].id,
        IND_PRICE: 0
      }
      addAllinvoiceDetails.value.push(newInvoiceDetail)
    }
  }
  return {
    updateInvoiceDetail,
    dialogDelete,
    dialog,
    loading,
    invoiceDetails,
    initInvoiceDetail,
    editedStockDetail,
    getInvoiceDetails,
    getInvoiceDetailsByInvoiceId,
    saveInvoiceDetail,
    editInvoiceDetail,
    sendToDelete,
    deleteItemConfirm,
    closeDelete,
    closeDialog,
    getMaterialName,
    getMaterialType,
    getMinimum,
    addinvoiceDetails,
    getAmount,
    getMat,
    addinvoiceDetail,
    updateMatDetail,
    clearAddDetails,
    addAllMat,
    addAllinvoiceDetails,
    updateMatAllDetail,
    clearAddAllMat
  }
})
