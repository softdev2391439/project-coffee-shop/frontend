import { ref, computed, nextTick, reactive, type Ref } from 'vue'
import { defineStore } from 'pinia'
import checkMemberService from '@/services/checkMember'
import type { User } from '@/types/User'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import checkInOut from '@/services/checkInOut'
import { useUserStore } from './user'
import { useAuthStore } from './auth'
import { useRouter } from 'vue-router'
import { useMapStore } from './map'
import type { CheckMember } from '@/types/CheckMember'

export const useCheckMemberStore = defineStore('checkMember', () => {
  const mapStore = useMapStore()
  const checkmember = ref<CheckMember>()
  const checkMembers = ref<CheckMember[]>([])
  const dialog = ref(false)
  const loading = ref(false)
  const dialogDelete = ref(false)
  const loginDialog = ref(false)
  const logoutDialog = ref(false)
  const from = ref(false)
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const mapDialog = ref(false)
  const filterDialog = ref(false)
  const initialCheckmember: CheckMember = {
    email: '',
    password: '',
    fristName: '',
    lastName: '',
    timeIn: new Date(),
    timeOut: new Date(),
    status: 'Absent',
    totalHour: 0,
    date: new Date()
  }

  const editedCheckmember = ref<CheckMember>(JSON.parse(JSON.stringify(initialCheckmember)))
  let editedIndex = -1


  async function getCheckMembers() {
    loadingStore.doLoad()
    const res = await checkMemberService.getCheckMembers()
    checkMembers.value = res.data
    loadingStore.finish()
  }
  async function save() {
    await saveCheckMember()
  }

  async function saveCheckMember() {
    try {
      loadingStore.doLoad()
      const member = editedCheckmember.value
      if (!member.id) {
        const res = await checkMemberService.addCheckMember(member)
      } else {
        const res = await checkMemberService.updateCheckMember(member)
      }
      await getCheckMembers()
      loadingStore.finish()
    } catch (error) {
      console.error('Error filtering checkInOuts:', error)
    }
  }

  function closeDialog() {
    loginDialog.value = false
    logoutDialog.value = false
    nextTick(() => {
      clearForm()
    })
  }

  function closeDelete() {
    dialogDelete.value = false
    clearForm()
  }

  async function deleteItemConfirm() {
    await deleteCheckInOut()
    closeDelete()
  }

  async function deleteCheckInOut() {
    loadingStore.doLoad()
    const checkmember = editedCheckmember.value
    const res = await checkMemberService.delCheckMember(checkmember)
    clearForm()
    await getCheckMembers()
    loadingStore.finish()
  }

  async function deleteItem(item: CheckMember) {
    if (!item.id) return
    await getCheckMember(item.id)
    dialogDelete.value = true
    editedIndex = -1
  }

  async function getCheckMember(id: number) {
    loadingStore.doLoad()
    const res = await checkMemberService.getCheckMember(id)
    editedCheckmember.value = res.data
    loadingStore.finish()
  }
  function onSubmit() { }
  function close() {
    dialog.value = false
    nextTick(() => {
      editedCheckmember.value = Object.assign({}, initialCheckmember)
      editedIndex = -1
    })
  }

  async function editItem(item: CheckMember) {
    if (!item.id) return
    await getCheckMember(item.id)
    dialog.value = true
    editedIndex = -1
  }

  function clearForm() {
    editedCheckmember.value = JSON.parse(JSON.stringify(initialCheckmember))
  }

  return {
    dialog,
    dialogDelete,
    loginDialog,
    checkMembers,
    from,
    editedCheckmember,
    loading,
    logoutDialog,
    mapDialog,
    save,
    closeDialog,
    closeDelete,
    deleteItemConfirm,
    deleteItem,
    onSubmit,
    close,
    deleteCheckInOut,
    getCheckMembers,
    saveCheckMember,
    clearForm,
    editItem,
    getCheckMember,
    checkmember
  }
})
