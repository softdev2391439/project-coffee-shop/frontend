import { useLoadingStore } from './loading'
import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'
import branchService from '@/services/branch'
import type { Branch } from '@/types/Branch'
import { useMapStore } from './map'

export const useBranchStore = defineStore('branch', () => {
  const loadingStore = useLoadingStore()
  const branches = ref<Branch[]>([])
  const loading = ref(false)
  const dialog = ref(false)
  const newBranchdialog = ref(false)
  const dialogDelete = ref(false)
  const from = ref(false)
  const mapStore = useMapStore()
  const initialBranch: Branch = {
    name: '',
    address: '180 หมู่ 2 ต.บางแสน',
    city: 'บางแสน',
    province: 'ชลบุรี',
    country: 'ไทย',
    postalcode: '20131',
    latitude: 13.281264959275443,
    longitude: 100.92410270978652
  }
  const editedBranch = ref<Branch>(JSON.parse(JSON.stringify(initialBranch)))
  let editedIndex = -1
  async function getBranch(id: number) {
    loadingStore.doLoad()
    const res = await branchService.getBranch(id)
    const branchDB = {
      id: res.data.BRANCH_ID,
      name: res.data.BRANCH_NAME,
      address: res.data.BRANCH_ADDRESS,
      city: res.data.BRANCH_CITY,
      province: res.data.BRANCH_PROVINCE,
      country: res.data.BRANCH_COUNTRY,
      postalcode: res.data.BRANCH_POSTAL_CODE,
      latitude: res.data.BRANCH_LATITUDE,
      longitude: res.data.BRANCH_LONGITUDE
    }
    editedBranch.value = branchDB
    loadingStore.finish()
  }

  async function save() {
    await saveBranch()
    closeDialog()
  }

  async function getBranches() {
    try {
      loadingStore.doLoad()
      const res = await branchService.getBranches()
      if (Array.isArray(res.data)) {
        const branchDB = res.data.map((dbBranch: any) => {
          return {
            id: dbBranch.BRANCH_ID,
            name: dbBranch.BRANCH_NAME,
            address: dbBranch.BRANCH_ADDRESS,
            city: dbBranch.BRANCH_CITY,
            province: dbBranch.BRANCH_PROVINCE,
            country: dbBranch.BRANCH_COUNTRY,
            postalcode: dbBranch.BRANCH_POSTAL_CODE,
            latitude: dbBranch.BRANCH_LATITUDE,
            longitude: dbBranch.BRANCH_LONGITUDE,
          }
        })
        branches.value = branchDB
      }

      loadingStore.finish()
    } catch (error) {
      console.error('Error:', error)
      loadingStore.finish()
    }
  }

  async function saveBranch() {
    loadingStore.doLoad()
    const branch = editedBranch.value
    if (!branch.id) {
      const res = await branchService.addBranch(branch)
    } else {
      const res = await branchService.updateBranch(branch)
    }

    clearForm()
    await getBranches()
    loadingStore.finish()
  }
  async function deleteBranch() {
    loadingStore.doLoad()
    const branch = editedBranch.value
    const res = await branchService.delBranch(branch)
    clearForm()
    await getBranches()
    loadingStore.finish()
  }

  async function editItem(item: Branch) {

    if (!item.id) return
    await getBranch(item.id)
    if (
      editedBranch.value &&
      editedBranch.value.latitude !== null &&
      editedBranch.value.longitude !== null
    ) {
      if (mapStore.branchAddress === null) {
        mapStore.branchAddress = { lat: 0, lng: 0, name: '' }
      }
      mapStore.branchAddress.lat = editedBranch.value.latitude
      mapStore.branchAddress.lng = editedBranch.value.longitude
      mapStore.branchAddress.name = editedBranch.value.name

    }
    dialog.value = true

  }

  function onSubmit() { }

  function close() {
    dialog.value = false
    nextTick(() => {
      editedBranch.value = Object.assign({}, initialBranch)
      editedIndex = -1
    })
  }

  async function deleteItem(item: Branch) {
    if (!item.id) return
    await getBranch(item.id)
    dialogDelete.value = true
    editedIndex = -1
  }

  function shownewDialog() {
    dialog.value = true
  }

  async function deleteItemConfirm() {
    await deleteBranch()
    closeDelete()
  }

  function closeDialog() {
    dialog.value = false
    clearForm()
  }

  function closeDelete() {
    dialogDelete.value = false
    clearForm()
  }

  function clearForm() {
    editedBranch.value = JSON.parse(JSON.stringify(initialBranch))
    if (mapStore.currentMarker) {
      mapStore.map.removeLayer(mapStore.currentMarker);
    }
    if (mapStore.currentLayer) {
      mapStore.map.removeLayer(mapStore.currentLayer);
    }
    if (mapStore.flageMarker) {
      mapStore.map.removeLayer(mapStore.flageMarker);
    }
    if (mapStore.brandMarker) {
      mapStore.map.removeLayer(mapStore.brandMarker);
    }
    mapStore.branchAddress = null


  }
  return {
    branches,
    getBranches,
    saveBranch,
    deleteBranch,
    editedBranch,
    getBranch,
    clearForm,
    shownewDialog,
    deleteItemConfirm,
    deleteItem,
    close,
    onSubmit,
    editItem,
    save,
    editedIndex,
    from,
    loading,
    newBranchdialog,
    dialog,
    closeDialog,
    dialogDelete,
    closeDelete
  }
})
