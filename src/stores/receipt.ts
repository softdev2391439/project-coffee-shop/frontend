import { ref, watch } from 'vue'
import { defineStore } from 'pinia'

import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import { useMemberStore } from './member'
import { usePromotionStore } from './promotion'
import receiptService from '@/services/receipt'
import { useAuthStore } from './auth'

export const useReceiptStore = defineStore('receipt', () => {
  const promotionStore = usePromotionStore()
  const memberStore = useMemberStore()
  const authStore = useAuthStore()
  const receiptDialog = ref(false)
  const QRpaymentDialog = ref(false)
  const receipt = ref<Receipt>()
  const receipts = ref<Receipt[]>([])
  const receiptItems = ref<ReceiptItem[]>([])
  const receiptItem = ref<ReceiptItem[]>
  const selectPaymentOption = ref('cash')
  const sweet = ref('')
  const type = ref('')
  const size = ref('')
  initReceipt()
  function initReceipt() {
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      promotionDiscount: 0,
      totalPrice: 0,
      discount: 0,
      income: 0,
      pointUse: 0,
      qty: 0,
      change: 0,
      paymentType: 'cash',
      userId: authStore.getCurrentUser()!.id!,
      memberId: memberStore.getCurrentMember()?.id!,
      branchId: authStore.getCurrentUser()!.branch.id!,
      promotionId: promotionStore.getCurrentPromotion()?.id!,
      member: memberStore.getCurrentMember()!,
      user: authStore.getCurrentUser()!,
      promotion: promotionStore.getCurrentPromotion()!
    }
    receiptItems.value = []
  }

  watch(
    receiptItems,
    () => {
      calReceipt()
    },
    { deep: true }
  )
  const paymentTypeDialog = ref(false)

  const addReceiptItem = (newReceiptItem: ReceiptItem) => {
    receiptItems.value.push(newReceiptItem)
  }

  const removeReceiptItem = (selectedItem: ReceiptItem) => {
    const index = receiptItems.value.findIndex((item) => item === selectedItem)
    receiptItems.value.splice(index, 1)
  }
  const inc = (selectedItem: ReceiptItem) => {
    selectedItem.unit++
  }
  const dec = (selectedItem: ReceiptItem) => {
    selectedItem.unit--
    if (selectedItem.unit === 0) {
      removeReceiptItem(selectedItem)
    }
  }
  const removeItem = (item: ReceiptItem) => {
    const index = receiptItems.value.findIndex((ri) => ri === item)
    receiptItems.value.splice(index, 1)
  }

  function updateDiscount(discount: number) {
    receipt.value!.promotionDiscount = discount
  }

  function updateLastDiscount(discount: number) {
    receipt.value!.discount = discount
  }

  function getProID(id: number) {
    receipt.value!.promotionId = id
  }

  function updateLastPrice(discount: number) {
    receipt.value!.totalPrice = discount
  }

  function getPoint(point: number) {
    receipt.value!.pointUse = point
  }

  function calPro(totalBefore: number, discount: number) {
    if (discount > 0) {
      receipt.value!.totalPrice = totalBefore - discount
    } else {
      receipt.value!.totalPrice = totalBefore
    }
  }

  const calReceipt = function () {
    let totalBefore = 0
    let totalDiscount = 0
    let totalQty = 0

    for (let i = 0; i < receiptItems.value.length; i++) {
      const item = receiptItems.value[i]
      totalBefore += item.price * item.unit
      totalQty += item.unit
    }

    const currentMember = memberStore.getCurrentMember()
    if (currentMember) {
      totalDiscount = totalBefore * 0.15

      if (currentMember.point && currentMember.point >= 10) {
        currentMember.point -= 0
      }
    }

    receipt.value!.totalPrice = totalBefore - totalDiscount
    receipt.value!.discount = totalDiscount
    receipt.value!.qty = totalQty
  }

  function showPaymentDialog() {
    paymentTypeDialog.value = true
  }

  function showReceiptDialog() {
    receipt.value!.receiptDetails = receiptItems.value
    receiptDialog.value = true
  }
  function clear() {
    receiptItems.value = []
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      totalPrice: 0,
      discount: 0,
      income: 0,
      qty: 0,
      change: 0,
      paymentType: 'cash',
      userId: 0,
      memberId: memberStore.getCurrentMember()!.id!,
      branchId: 0,
      promotionId: 0,
      member: memberStore.getCurrentMember()!
    }
    promotionStore.resetSelectedPromotions()
    memberStore.clear()
  }
  const receiptOrder = async () => {
    try {
      if (receipt.value?.member) {
        const memberId = receipt.value.member.id
        receipt.value!.memberId = memberId !== undefined ? memberId : -1
      } else {
        receipt.value!.memberId = -1
      }

      if (receipt.value?.user) {
        const userId = receipt.value.user.id
        receipt.value!.userId = userId !== undefined ? userId : -1
      } else {
        receipt.value!.userId = -1
      }

      if (receipt.value?.promotion) {
        const promotionId = receipt.value.promotion.id
        receipt.value!.promotionId = promotionId !== undefined ? promotionId : -1
      } else {
        receipt.value!.promotionId = -1
      }


      await receiptService.addReceiptOrder(receipt.value!, receiptItems.value)

      initReceipt()
    } catch (e) {
      console.error('Error:', e)
    }
  }
  async function getReceipt() {
    const res = await receiptService.getReceipts()
    receipts.value = res.data
  }

  async function getReceipts(id: number) {
    try {
      const res = await receiptService.getReceipt(id)
      receipt.value = res.data
    } catch (error) {
      return null
    }
  }
  async function getMaxTotalReceipt() {
    try {
      const res = await receiptService.getMaxTotalReceipt()
      receipt1.value = res.data

    } catch (e: any) {
      console.log(e.message)
    }
  }

  const receipt1 = ref<Receipt[]>([])

  return {
    receiptItems,
    receipt,
    receiptDialog,
    QRpaymentDialog,
    receiptOrder,
    showPaymentDialog,
    showReceiptDialog,
    paymentTypeDialog,
    addReceiptItem,
    removeReceiptItem,
    inc,
    dec,
    removeItem,
    calReceipt,
    updateDiscount,
    calPro,
    clear,
    selectPaymentOption,
    sweet,
    receiptItem,
    type,
    size,
    getReceipt,
    getReceipts,
    receipts,
    updateLastPrice,
    updateLastDiscount,
    getPoint,
    getProID,
    getMaxTotalReceipt
  }
})
