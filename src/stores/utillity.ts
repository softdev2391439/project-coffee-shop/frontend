import { nextTick, ref } from 'vue'
import { useLoadingStore } from './loading'
import utillityService from '@/services/utillity'
import type { Utillity } from '@/types/Utillity'
import http from '@/services/http'

export function useUtillityStore() {
  const utillities = ref<Utillity[]>([])
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const from = ref(false)
  const loadindStore = useLoadingStore()
  const loading = ref(false)

  let editedIndex = -1
  const initialUtillity: Utillity & { files: File[] } = {
    picture: 'nobill.jpg', //ใส่รูป
    dateAdded: new Date(),
    price: 0,
    type: 'Water bill',
    detail: '',
    files: []
  }

  const editedUtillity = ref<Utillity & { files: File[] }>(
    JSON.parse(JSON.stringify(initialUtillity))
  )


  async function getUtillities() {
    try {
      loadindStore.doLoad()
      const res = await utillityService.getUtillities()
      utillities.value = res.data
      loadindStore.finish()
    } catch (e: any) {
      loadindStore.finish()
      console.log(e.message)
    }
  }

  async function save() {
    await saveUtillity()
    closeDialog()
  }

  async function saveUtillity() {
    try {
      loadindStore.doLoad()
      const utillity = editedUtillity.value
      console.log(1)
      if (!utillity.id) {
        const res = await utillityService.addUtillity(utillity)
      } else {
        const res = await utillityService.updateUtillity(utillity)
      }
      await getUtillities()
      loadindStore.finish()
      console.log(2)
    } catch (e: any) {
      loadindStore.finish()
      console.log(e.message)

    }
  }

  function closeDialog() {
    dialog.value = false
    clearForm()
  }
  function closeDelete() {
    dialogDelete.value = false
    clearForm()
  }

  async function deleteItemConfirm() {
    await deleteUtillity()
    closeDelete()
  }

  async function deleteUtillity() {
    loadindStore.doLoad()
    const utillity = editedUtillity.value
    const res = await utillityService.deleteUtillity(utillity)
    try {
      await getUtillities()
      loadindStore.finish()
    } catch (e: any) {
      loadindStore.finish()
      console.log(e.message)
    }
  }

  async function deleteItem(item: Utillity) {
    if (!item.id) return
    await getUtillity(item.id)
    dialogDelete.value = true
    editedIndex = -1
  }

  function showloginDialog() {
    dialog.value = true
  }

  function onSubmit() { }
  // function close() {
  //   dialog.value = false
  //   nextTick(() => {
  //     editedUtillity.value = Object.assign({}, initialUtillity)
  //     editedIndex = -1
  //   })
  // }

  async function editItem(item: Utillity) {
    if (!item.id) return
    await getUtillity(item.id)
    dialog.value = true
  }

  async function getUtillity(id: number) {
    try {
      loadindStore.doLoad()
      const res = await utillityService.getUtillity(id)
      editedUtillity.value = res.data
      loadindStore.finish()
    } catch (e: any) {
      loadindStore.finish()
      console.log(e.message)
    }
  }

  function clearForm() {
    editedUtillity.value = JSON.parse(JSON.stringify(initialUtillity))
  }

  return {
    dialog,
    dialogDelete,
    from,
    loading,
    editedUtillity,
    utillities,
    showloginDialog,
    save,
    closeDialog,
    closeDelete,
    deleteItemConfirm,
    deleteItem,
    onSubmit,
    close,
    editItem,
    getUtillities,
    getUtillity,
    clearForm,
  }
}
