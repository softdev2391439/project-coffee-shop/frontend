import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'


import { useMaterialStore } from './material'
import { useUserStore } from './user'
import type { Invoice } from '@/types/Invoice/Invoice'

import type { Material } from '@/types/Material'

import http from '@/services/http'
import invoiceService from '@/services/invoice'
import { useLoadingStore } from '@/stores/loading'

import stockService from '@/services/invoice'
import { useBranchStore } from './branch'
import material from '@/services/material'
import type { InvoiceDetail } from '@/types/Invoice/InvoiceDetail'
import { useInvoiceDetailStore } from './invoiceDetail'

export const useInvoiceStore = defineStore('invoiceStore', () => {

  const userStore = useUserStore()
  const materialStore = useMaterialStore()

  const loadingStore = useLoadingStore()
  const branchStore = useBranchStore()
  const invoiceDetailStore = useInvoiceDetailStore()

  const addAllDialog = ref(false)
  const dialogDelete = ref(false)
  const editItemDialog = ref(false)
  const addNewMaterialDialog = ref(false)
  const StockDialog = ref(false)
  const saveMaterialDialog = ref(false)
  const addNewStockItemDialog = ref(false)
  const addSomeDialog = ref(false)
  const amount = ref(0)
  const loading = ref(false)
  const place = ref('')

  const initStock = ref<Invoice>({
    INV_ID: -1,
    INV_PLACE: '',
    INV_PRICE: 0,
    INV_BRANCH: -1,
    INV_USER: -1,
    INV_INVOICEDETAILS: []
  })

  const invoices = ref<Invoice[]>([])

  const editedInvoice = ref<Invoice>(initStock.value)

  const initInvoiceDetail: InvoiceDetail = {
    IND_ID: -1,
    IND_MATERIAL: -1,
    IND_AMOUT: 0,
    IND_PRICE: 0,
    IND_INVOICE: -1
  }

  const invoiceDetails = ref<InvoiceDetail[]>([])
  const editStockItem = ref<InvoiceDetail>(JSON.parse(JSON.stringify(initInvoiceDetail)))

  function initializeItem() {
    invoiceDetails.value = []
  }

  function getUserName(userID: number | undefined) {
    console.log(userID)
    const user = userStore.users.find((user) => user && user.id === userID)
    return user ? user.name : 'Unknown User'
  }

  function getBranchName(branchID: number | undefined) {
    const branch = branchStore.branches.find((branch) => branch && branch.id === branchID)
    return branch ? branch.name : 'Unknown Branch'
  }

  function formatDate(date: Date) {
    const year = date.getFullYear().toString().padStart(4, '0')
    const month = (date.getMonth() + 1).toString().padStart(2, '0')
    const day = date.getDate().toString().padStart(2, '0')
    const hours = date.getHours().toString().padStart(2, '0')
    const minutes = date.getMinutes().toString().padStart(2, '0')
    const seconds = date.getSeconds().toString().padStart(2, '0')

    const formattedDate = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`
    return formattedDate
  }
  function close() {
    invoiceDetails.value = []
    addNewStockItemDialog.value = false
    addSomeDialog.value = false
    addAllDialog.value = false
  }



  function sendToDelete(invoice: Invoice) {
    editedInvoice.value = invoice
    dialogDelete.value = true
  }

  async function getInvoices() {
    loadingStore.doLoad()
    try {
      const res = await invoiceService.getInvoices()
      const invoicesData = res.data.map((invoice: any) => {
        let userID = -1

        if (invoice.user && invoice.user.id) {
          userID = invoice.user.id
        }
        return {
          INV_ID: invoice.INV_ID,
          INV_PLACE: invoice.INV_PLACE,
          INV_PRICE: invoice.INV_PRICE,
          INV_BRANCH: invoice.INV_BRANCH.id,
          INV_USER: invoice.INV_USER.id,
          INV_INVOICEDETAILS: invoice.INV_INVOICEDETAILS
        }
      })
      invoices.value = invoicesData
      loadingStore.finish()
    } catch (e) {
      console.log('cant fetch data from database')
      loadingStore.finish()
    }
  }

  async function deleteItemConfirm(invoice: Invoice) {
    await deleteInvoice(invoice)
    closeDelete()
  }

  async function deleteInvoice(invoice: Invoice) {
    loadingStore.doLoad()
    const res = await invoiceService.delInvoice(invoice)
    await getInvoices()
    loadingStore.finish()
  }

  function closeDelete() {
    dialogDelete.value = false
    editedInvoice.value = initStock.value
  }

  function editStock(item: Invoice) {
    editedInvoice.value = Object.assign({}, item)
    addAllDialog.value = true
  }

  async function saveInvoice(invoice: Invoice) {
    loadingStore.doLoad()

    console.log('nus')
    if (invoice.INV_ID < 0) {
      const res = await invoiceService.addInvoice(invoice)
      console.log('nuss')
    } else {
      const res = await invoiceService.updateInvoice(invoice)
    }

    invoiceDetailStore.clearAddDetails()
    place.value = ''
    getInvoices()
    loadingStore.finish()
  }

  function closeDialog() {
    addAllDialog.value = false
    addSomeDialog.value = false
    nextTick(() => {
      editedInvoice.value = Object.assign({}, initStock.value)
      materialStore.addsomeMaterial = []
    })
  }

  function InvoicePrice(invoiceDetails: InvoiceDetail[]) {
    let total = 0
    for (let i = 0; i < invoiceDetails.length; i++) {
      total = total + invoiceDetails[i].IND_PRICE
    }
    return total
  }

  return {
    invoiceDetails,
    addAllDialog,
    dialogDelete,
    editStockItem,
    deleteItemConfirm,
    editStock,
    deleteInvoice,
    closeDelete,
    initInvoiceDetail,
    initializeItem,
    editItemDialog,
    addNewMaterialDialog,
    saveMaterialDialog,
    invoices,
    getUserName,
    getBranchName,
    formatDate,
    StockDialog,
    addNewStockItemDialog,
    addSomeDialog,
    amount,
    close,
    editedInvoice,
    getInvoices,
    saveInvoice,
    closeDialog,
    loading,
    sendToDelete,
    InvoicePrice,
    place
  }
})
