import { nextTick, ref } from 'vue'
import salaryService from '@/services/salary'

import type { Salary } from '@/types/Salary'

import { useLoadingStore } from './loading'
import { useMessageStore } from './message'

export function useSalaryStore() {
  const PaymentDialog = ref(false)
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const SlipDialog = ref(false)
  const form = ref(false)
  const newUser = ref(false)

  const salarys = ref<Salary[]>([])
  const payment = ref<Salary[]>([])

  const initialSalary: Salary = {
    fullName: '',
    role: '',
    totalHour: 0,
    pricePerHour: 0,
    totalPay: 0,
    status: 'not pay',
    branch: {
      id: 0,
      name: '',
      address: '',
      city: '',
      province: '',
      country: '',
      postalcode: '',
      latitude: 0,
      longitude: 0
    },
    payDate: new Date(),
    month: 0,
    TotalPayment: 0
  }

  const editedSalary = ref<Salary>({
    fullName: '',
    role: '',
    totalHour: 0,
    pricePerHour: 0,
    totalPay: 0,
    status: 'not pay',
    branch: {
      id: 0,
      name: '',
      address: '',
      city: '',
      province: '',
      country: '',
      postalcode: '',
      latitude: 0,
      longitude: 0
    },
    TotalPayment: 0,
    payDate: new Date(),
    month: 0
  })
  let editedIndex = -1

  const messageStore = useMessageStore()
  const loadindStore = useLoadingStore()
  const loading = ref(false)

  const selectedSalary = ref<Salary>(JSON.parse(JSON.stringify(initialSalary)))

  const selected = ref<Salary>(JSON.parse(JSON.stringify(initialSalary)))
  const res = ref<Salary>()

  async function getSalarys() {
    loadindStore.doLoad()
    const res = await salaryService.getSalarys()
    const salaryData = res.data.map((salary: any) => {
      return {
        id: salary.id,
        fullName: salary.user.name,
        role: salary.user.role.name,
        totalHour: salary.totalHour,
        pricePerHour: salary.pricePerHour,
        totalPay: salary.totalPay,
        status: salary.status,
        branch: salary.branch.id,
        payDate: salary.payDate,
        month: salary.month,
        user: salary.user.id
      }
    })
    salarys.value = salaryData
    loadindStore.finish()
  }

  async function save() {
    await saveSalary()
    closeDialog()
  }

  async function saveSalary() {
    try {
      loadindStore.doLoad()
      const salary = editedSalary.value
      if (!salary.id) {
        const res = await salaryService.addSalary(salary)
      } else {
        const res = await salaryService.updateSalary(salary)
      }

      clearForm()
      await getSalarys()
      loadindStore.finish()
    } catch (e: any) {
      loadindStore.finish()
      messageStore.showMessage(e.message)
    }
  }

  function closeDialog() {
    SlipDialog.value = false
    clearForm()
  }

  function closeDelete() {
    dialogDelete.value = false
    clearForm()
  }

  async function deleteItemConfirm() {
    await deleteSalary()
    closeDelete()
  }

  async function deleteSalary() {
    loadindStore.doLoad()
    const salary = editedSalary.value
    const res = await salaryService.delSalary(salary)
    clearForm()
    await getSalarys()
    loadindStore.finish()
  }

  async function deleteItem(item: Salary) {
    if (!item.id) return
    await getSalary(item.id)
    dialogDelete.value = true
    editedIndex = -1
  }

  function onSubmit() {}

  function close() {
    dialog.value = false
    nextTick(() => {
      editedSalary.value = Object.assign({}, initialSalary)
      editedIndex = -1
    })
  }

  async function editItem(item: Salary) {
    if (!item.id) return
    await getSalary(item.id)
    dialog.value = true
    editedIndex = -1
  }

  async function getSalary(id: number) {
    loadindStore.doLoad()
    const res = await salaryService.getSalary(id)
    editedSalary.value = res.data
    loadindStore.finish()
  }

  function clearForm() {
    editedSalary.value = JSON.parse(JSON.stringify(initialSalary))
  }

  const showPayroll = (item: Salary) => {
    PaymentDialog.value = true
    if (item.status == 'not pay') {
      selected.value = item
      PaymentDialog.value = true
    }
  }

  async function getTotalPay() {
    try {
      const res = await salaryService.getTotalpay()
      payment.value = res.data
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
  }

  async function getTotalPayByMonth() {
    try {
      const res = await salaryService.getTotalpayByMonth()
      salarys.value = res.data
    } catch (e: any) {
      messageStore.showMessage(e.message)
    }
  }

  const confirmPayment = async () => {
    try {
      loadindStore.doLoad()
      if (selected.value.status === 'not pay') {
        const newSalary = {
          totalHour: 0,
          pricePerHour: 45,
          totalPay: 0,
          status: 'not pay',
          branch: selected.value.branch,
          month: selected.value.month + 1,
          user: selected.value.user
        }

        await salaryService.addSalary(newSalary)

        selected.value.status = 'pay'

        await salaryService.updateSalary(selected.value)
        getSalarys()
        loadindStore.finish()
      }
    } catch (error) {
      console.error('Error confirming payment:', error)
    }
  }

  return {
    PaymentDialog,
    dialog,
    SlipDialog,
    dialogDelete,
    form,
    salarys,
    editedSalary,
    selected,
    loading,
    initialSalary,
    selectedSalary,
    showPayroll,
    confirmPayment,
    getSalarys,
    save,
    saveSalary,
    closeDialog,
    closeDelete,
    deleteItemConfirm,
    deleteSalary,
    deleteItem,
    onSubmit,
    close,
    editItem,
    getSalary,
    clearForm,
    newUser,
    res,
    getTotalPay,
    getTotalPayByMonth,
    payment
  }
}
