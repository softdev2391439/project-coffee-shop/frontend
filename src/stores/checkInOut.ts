import { ref, computed, nextTick, reactive, type Ref } from 'vue'
import { defineStore } from 'pinia'
import type { CheckInOut } from '@/types/CheckInOut'
import checkInOutService from '@/services/checkInOut'
import userService from '@/services/user'
import type { User } from '@/types/User'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import checkInOut from '@/services/checkInOut'
import { useUserStore } from './user'
import { useAuthStore } from './auth'
import { useRouter } from 'vue-router'
import { useMapStore } from './map'

export const useCheckInOutStore = defineStore('checkinout', () => {
  const authStore = useAuthStore()
  const userStore = useUserStore()
  const mapStore = useMapStore()
  const checkinout = ref<CheckInOut>()
  const checkInOuts = ref<CheckInOut[]>([])
  const dialog = ref(false)
  const loading = ref(false)
  const dialogDelete = ref(false)
  const loginDialog = ref(false)
  const logoutDialog = ref(false)
  const from = ref(false)
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const mapDialog = ref(false)
  const filterDialog = ref(false)
  const initialCheckinout: CheckInOut = {
    email: '',
    password: '',
    fullName: '',
    timeIn: new Date(),
    timeOut: new Date(),
    status: 'Absent',
    totalHour: 0,
    date: new Date()
  }

  const editedCheckinout = ref<CheckInOut>(JSON.parse(JSON.stringify(initialCheckinout)))
  let editedIndex = -1


  async function getCheckInOuts() {
    loadingStore.doLoad()
    const res = await checkInOutService.getCheckInOuts()
    checkInOuts.value = res.data
    loadingStore.finish()
  }
  async function save() {
    await saveCheckinout()
  }

  async function saveCheckinout() {
    try {
      loadingStore.doLoad()
      const dbUser = await userService.getUserByEmail(editedCheckinout.value.email)
      const user = dbUser.data
      const branch = ref(user.branch)
      if (branch.value && branch.value.latitude !== null && branch.value.longitude !== null) {
        if (mapStore.branchAddress === null) {
          mapStore.branchAddress = { lat: 0, lng: 0, name: '' }
        }
        mapStore.branchAddress.lat = branch.value.latitude
        mapStore.branchAddress.lng = branch.value.longitude
        mapStore.branchAddress.name = branch.value.name
        mapStore.useIn = 'checkUser'
        mapDialog.value = true
        const intervalId = setInterval(async () => {
          if (mapStore.checkUser) {
            clearInterval(intervalId)
            mapDialog.value = false
            const currentTimeInThaiTimeZone = new Date().toLocaleString('en-us', {
              timeZone: 'Asia/Bangkok'
            })
            const checkInout = editedCheckinout.value
            closeDialog()
            loadingStore.doLoad()
            if (!checkInout.id) {
              editedCheckinout.value.status = 'Present'
              const res = await checkInOutService.addCheckInOut(checkInout)
            } else {
              editedCheckinout.value.timeOut = new Date(currentTimeInThaiTimeZone)
              editedCheckinout.value.status = 'Absent'
              const res = await checkInOutService.updateCheckInOut(checkInout)
            }
            await getCheckInOuts()
          }
        }, 1000)
        setTimeout(() => {
          clearInterval(intervalId)
          if (!mapStore.checkUser) {
            messageStore.showMessage('ที่อยู่ไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง')
            mapDialog.value = false
          }
        }, 10000)
      }
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }

  function closeDialog() {
    loginDialog.value = false
    logoutDialog.value = false
    nextTick(() => {
      clearForm()
    })
  }

  function closeDelete() {
    dialogDelete.value = false
    clearForm()
  }

  async function deleteItemConfirm() {
    await deleteCheckInOut()
    closeDelete()
  }

  async function deleteCheckInOut() {
    loadingStore.doLoad()
    const checkInOut = editedCheckinout.value
    const res = await checkInOutService.delCheckInOut(checkInOut)
    clearForm()
    await getCheckInOuts()
    loadingStore.finish()
  }

  async function deleteItem(item: CheckInOut) {
    if (!item.id) return
    await getCheckInOut(item.id)
    dialogDelete.value = true
    editedIndex = -1
  }

  async function getCheckInOut(id: number) {
    loadingStore.doLoad()
    const res = await checkInOutService.getCheckInOut(id)
    editedCheckinout.value = res.data
    loadingStore.finish()
  }
  function onSubmit() { }
  function close() {
    dialog.value = false
    nextTick(() => {
      editedCheckinout.value = Object.assign({}, initialCheckinout)
      editedIndex = -1
    })
  }

  async function editItem(item: CheckInOut) {
    logoutDialog.value = true
    if (!item.id) return
    await getCheckInOut(item.id)
    dialog.value = true
    editedIndex = -1
  }

  function clearForm() {
    editedCheckinout.value = JSON.parse(JSON.stringify(initialCheckinout))
  }



  async function fetchDataByMonthAndYear(month: number, year: number) {
    try {
      const response = await checkInOutService.getByFilterMount(month, year)
      const data = response.data

      // ตรวจสอบข้อมูลที่ได้รับว่ามีการแสดงผลและแปลงให้เป็นวัตถุ Date ได้ถูกต้องหรือไม่
      const newData = data.map((item: any) => ({
        id: item.id,
        email: item.email,
        password: item.password,
        fullName: item.fullName,
        timeIn: new Date(item.timeIn), // แปลงเป็นวัตถุ Date
        timeOut: item.timeOut ? new Date(item.timeOut) : null // แปลงเป็นวัตถุ Date หรือ null หากข้อมูล timeOut ไม่มี
      }))

      // ตั้งค่าข้อมูลใหม่ในตัวแปร checkInOuts
      checkInOuts.value = newData
      console.log(data)
    } catch (error) {
      console.error('Error fetching data by month and year:', error)
    }
  }


  return {
    dialog,
    dialogDelete,
    loginDialog,
    checkInOuts,
    from,
    editedCheckinout,
    loading,
    logoutDialog,
    mapDialog,
    save,
    closeDialog,
    closeDelete,
    deleteItemConfirm,
    deleteItem,
    onSubmit,
    close,
    deleteCheckInOut,
    getCheckInOuts,
    saveCheckinout,
    clearForm,
    editItem,
    getCheckInOut,
    checkinout,
    filterDialog,
    fetchDataByMonthAndYear
    // logIn,
    // getCurrentUser
  }
})
