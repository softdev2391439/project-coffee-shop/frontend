import type { CheckStock } from '@/types/CheckStock'
import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { CheckStockItem } from '@/types/CheckStockItem'
import { useLoadingStore } from './loading'
import checkStockService from '@/services/checkStock'
export const useCheckStockStore = defineStore('checkStock', () => {
  const loadingStore = useLoadingStore()
  const checkStockItems = ref<CheckStockItem[]>([])
  const checkStock = ref<CheckStock>({
    userId: 0,
    remainingAmount: 0,
    totalUse: 0
  })
  const editedCheckStock = ref<CheckStock>(JSON.parse(JSON.stringify(checkStock)))

  async function getCheckStock(id: number) {
    loadingStore.doLoad()
    const res = await checkStockService.getCheckStock(id)
    editedCheckStock.value = res.data
    loadingStore.finish()
  }
  async function getCheckStocks() {
    loadingStore.doLoad()
    const res = await checkStockService.getCheckStocks()
    checkStocks.value = res.data
    loadingStore.finish()
  }
  async function saveCheckStock() {
    loadingStore.doLoad()
    const checkStock = editedCheckStock.value
    if (!checkStock.id) {
      const res = await checkStockService.addCheckStock(checkStock)
    } else {
      const res = await checkStockService.updateCheckStock(checkStock)
    }
    clearFrom()
    await getCheckStocks()
    loadingStore.finish()
  }

  async function deleteCheckStock() {
    loadingStore.doLoad()
    const checkStock = editedCheckStock.value
    const res = await checkStockService.delCheckStock(checkStock)
    clearFrom()
    await getCheckStocks()
    loadingStore.finish()
  }

  const addCheckStockItem = (newCheckstockItem: CheckStockItem) => {
    checkStockItems.value.push(newCheckstockItem)
  }

  function clearFrom() {
    editedCheckStock.value = JSON.parse(JSON.stringify(initilCheckStock))
  }

  return {
    //checkStock,
    checkStockItems,
    getCheckStock,
    getCheckStocks,
    saveCheckStock,
    deleteCheckStock,
    addCheckStockItem
  }
})
