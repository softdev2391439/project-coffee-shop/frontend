import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import authService from '@/services/auth'
import { useMessageStore } from './message'
import { useRouter } from 'vue-router'
import { useLoadingStore } from './loading'
import { useMapStore } from './map'

export const useAuthStore = defineStore('auth', () => {
  const messageStore = useMessageStore()
  const router = useRouter()
  const loadingStore = useLoadingStore()
  const mapStore = useMapStore()
  const mapDialog = ref(false)

  const login = async function (email: string, password: string) {
    loadingStore.doLoad()
    try {
      const res = await authService.login(email, password)
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('access_token', res.data.access_token)
      const user = res.data.user
      const branch = ref(user.branch)
      if (branch.value && branch.value.latitude !== null && branch.value.longitude !== null) {
        if (mapStore.branchAddress === null) {
          mapStore.branchAddress = { lat: 0, lng: 0, name: '' }
        }
        mapStore.branchAddress.lat = branch.value.latitude
        mapStore.branchAddress.lng = branch.value.longitude
        mapStore.branchAddress.name = branch.value.name
        mapStore.useIn = 'checkUser'
        mapDialog.value = true
        const intervalId = setInterval(() => {
          if (mapStore.checkUser) {
            clearInterval(intervalId)
            mapDialog.value = false
            messageStore.showMessage('Login Success')
            router.replace('/')
          }
        }, 1000)
        setTimeout(() => {
          clearInterval(intervalId)
          if (!mapStore.checkUser) {
            messageStore.showMessage('ที่อยู่ไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง')
            logout()
            mapDialog.value = false
          }
        }, 10000)
      }
    } catch (e: any) {
      console.log(e)
      messageStore.showMessage(e.message)
    }
    loadingStore.finish()
  }

  const logout = function () {
    localStorage.removeItem('user')
    localStorage.removeItem('access_token')
    router.replace('/login')
  }

  function getCurrentUser(): User | null {
    const strUser = localStorage.getItem('user')
    if (strUser === null) return null
    const user = JSON.parse(strUser)
    if (!user || !user.image) return null
    return user
  }

  function getToken(): String | null {
    const strToken = localStorage.getItem('access_token')
    if (strToken === null) return null
    return JSON.parse(strToken)
  }

  return { getCurrentUser, login, logout, getToken, mapDialog }
})
