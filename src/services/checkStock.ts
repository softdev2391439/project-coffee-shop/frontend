import type { CheckStock } from '@/types/CheckStock'
import http from './http'

function addCheckStock(checkStock: CheckStock) {
  return http.post('/checkStocks', checkStock)
}

function updateCheckStock(checkStock: CheckStock) {
  return http.patch(`/checkStocks/${checkStock.id}`, checkStock)
}

function delCheckStock(checkStock: CheckStock) {
  return http.delete(`/checkStocks/${checkStock.id}`)
}

function getCheckStock(id: number) {
  return http.get(`/checkStocks/${id}`)
}
function getCheckStocks() {
  return http.get('/checkStocks')
}

export default { addCheckStock, updateCheckStock, delCheckStock, getCheckStock, getCheckStocks }
