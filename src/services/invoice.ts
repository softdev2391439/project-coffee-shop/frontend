import type { Invoice } from '@/types/Invoice/Invoice'
import http from './http'



function addInvoice(invoice: Invoice) {

  const { INV_ID, INV_INVOICEDETAILS, ...rest } = invoice

  const newInvoiceDetails = INV_INVOICEDETAILS.map(
    ({ IND_ID, IND_AMOUT, IND_INVOICE, IND_MATERIAL, IND_PRICE }) => ({
      IND_AMOUT: IND_AMOUT,
      IND_MATERIAL: IND_MATERIAL,
      IND_PRICE: IND_PRICE
    })
  )


  const dataToSend = {
    ...rest,
    INV_INVOICEDETAILS: newInvoiceDetails
  }

  return http.post('/invoices', dataToSend)
}
function updateInvoice(invoice: Invoice) {
  const { INV_ID, ...invoicewithOutId } = invoice
  return http.patch(`/invoices/${invoice.INV_ID}`, invoicewithOutId)
}
function delInvoice(invoice: Invoice) {
  return http.delete(`/invoices/${invoice.INV_ID}`)
}
function getInvoice(id: number) {
  return http.get(`/invoices/${id}`)
}
function getInvoices() {
  return http.get('/invoices')
}

export default { addInvoice, updateInvoice, delInvoice, getInvoice, getInvoices }
