import type { CheckMember } from '@/types/CheckMember'
import http from './http'

function addCheckMember(CheckMember: CheckMember) {
  return http.post('/checkmembers', CheckMember)
}

function updateCheckMember(CheckMember: CheckMember) {
  return http.patch(`/checkmembers/${CheckMember.id}`, CheckMember)
}

function delCheckMember(CheckMember: CheckMember) {
  return http.delete(`/checkmembers/${CheckMember.id}`)
}

function getCheckMember(id: number) {
  return http.get(`/checkmembers/${id}`)
}

function getCheckMembers() {
  return http.get('/checkmembers')
}

export default {
  addCheckMember,
  updateCheckMember,
  delCheckMember,
  getCheckMember,
  getCheckMembers
}
