import type { Stock } from '../types/Stock'
import http from './http'

function addStock(stock: Stock) {
  const { id, date, stockDetails, ...rest } = stock


  const newStockDetails = stockDetails.map(({ id, StockId, QOH, balance, materialId }) => ({
    SD_QOH: QOH,
    SD_BALANCE: balance,
    SD_MATERIAL: materialId
  }))


  const dataToSend = {
    ...rest,
    stockDetails: newStockDetails
  }

  console.log('Data to send: ', dataToSend)
  return http.post('/stocks', dataToSend)
}

function updateStock(stock: Stock) {
  return http.patch(`/stocks/${stock.id}`, stock)
}
function delStock(stock: Stock) {
  return http.delete(`/stocks/${stock.id}`)
}
function getStock(id: number) {
  return http.get(`/stocks/${id}`)
}
function getStocks() {
  return http.get('/stocks')
}
function GetStockDetailsByMonth(bodyDate: any) {
  if (
    bodyDate.month == '' ||
    bodyDate.year == '' ||
    bodyDate.month == null ||
    bodyDate.year == null
  ) {
    console.log('cant created graph!!!')
  } else {
    return http.post(`/stocks/stocks/month`, bodyDate)
  }
}

function GetStockDetailsByYear(bodyDate: any) {
  if (bodyDate.year == '' || bodyDate.year == null) {
    console.log('cant created graph!!!')
  } else {
    return http.post(`/stocks/stocks/year`, bodyDate)
  }
}

function GetStockDetailsSet(bodyDate: any) {
  if (
    bodyDate.month == '' ||
    bodyDate.year == '' ||
    bodyDate.month == null ||
    bodyDate.year == null
  ) {
    console.log('cant created graph!!!')
  } else {
    return http.post(`/stocks/stocks/set`, bodyDate)
  }
}

function GetStockDetailsMinusOne(bodyDate: any) {
  if (
    bodyDate.month == '' ||
    bodyDate.year == '' ||
    bodyDate.month == null ||
    bodyDate.year == null
  ) {
    console.log('cant created graph!!!')
  } else {
    return http.post(`/stocks/stocks/minusOne`, bodyDate)
  }
}
export default {
  addStock,
  updateStock,
  delStock,
  getStock,
  getStocks,
  GetStockDetailsByMonth,
  GetStockDetailsByYear,
  GetStockDetailsSet,
  GetStockDetailsMinusOne
}
