import { type Material } from '../types/Material'
import http from './http'

function addMaterial(material: Material) {
  const mat: Material = {
    id: material.id,
    name: material.name,
    type: material.type,
    minimum: material.minimum,
    amount: material.amount
  }
  console.log('add mat: ', mat)
  const { id, ...matwithOutid } = mat
  console.log('matwithOutid: ', matwithOutid)
  return http.post('/materials', matwithOutid)
}
function updateMaterial(material: Material) {
  const mat: Material = {
    id: material.id,
    name: material.name,
    type: material.type,
    minimum: material.minimum,
    amount: material.amount
  }
  console.log('update mat: ', mat)
  const { id, ...matwithOutid } = mat
  console.log('matwithOutid: ', matwithOutid)
  return http.patch(`/materials/${mat.id}`, matwithOutid)
}
function delMaterial(material: Material) {
  return http.delete(`/materials/${material.id}`)
}
function getMaterial(id: number) {
  return http.get(`/materials/${id}`)
}
function getMaterials() {
  return http.get('/materials')
}

export default { addMaterial, updateMaterial, delMaterial, getMaterial, getMaterials }
