import type { CheckInOut } from '@/types/CheckInOut'
import http from './http'

function addCheckInOut(checkInOut: CheckInOut) {
  return http.post('/checkInOuts', checkInOut)
}

function updateCheckInOut(checkInOut: CheckInOut) {
  console.log('checkInOut service: ', checkInOut)
  return http.patch(`/checkInOuts/${checkInOut.id}`, checkInOut)
}

function delCheckInOut(checkInOut: CheckInOut) {
  return http.delete(`/checkInOuts/${checkInOut.id}`)
}

function getCheckInOut(id: number) {
  return http.get(`/checkInOuts/${id}`)
}

function getCheckInOuts() {
  return http.get('/checkInOuts')
}

function getByFilter(id: number) {
  console.log('id: ' + id)
  return http.get(`/checkInOuts/filter/${id}`)
}

function getByFilterDate(date: Date) {
  console.log('date: ' + date)
  return http.get(`/checkInOuts/filter/${date}`)
}

function getByFilterMount(month: number, year: number) {
  return http.get(`/checkInOuts/filter/month/${month}/${year}`);
}


export default {
  addCheckInOut,
  updateCheckInOut,
  delCheckInOut,
  getCheckInOut,
  getCheckInOuts,
  getByFilter,
  getByFilterDate,
  getByFilterMount
}
