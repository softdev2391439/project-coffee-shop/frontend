import type { UtilityCost } from '@/types/utiliycost/UtilityCost'
import http from './http'

function addUtilityCost(utilityCost: UtilityCost) {
  return http.post('/utility-cost', utilityCost)
}
function delUtilityCost(utilityCost: UtilityCost) {
  return http.delete(`/utility-cost/${utilityCost.id}`)
}
function getUtilityCost(id: number) {
  return http.get(`/utility-cost/${id}`)
}

function getDtailonMonth(year: number, month: number, branch: string) {
  return http.get(`/utility-cost/getDtailByMonth/${year}/${month}/${branch}`)
}

function getYear() {
  return http.get(`/utility-cost/get/year`)
}

function getPriceOfAllMonth(year: number, branch: number) {
  return http.get(`/utility-cost/getpriceGroupMonth/${branch}/${year}`)
}

function getUtilityCostes() {
  console.log('getUtilityCostes')
  return http.get('/utility-cost')
}
export default {
  addUtilityCost,
  delUtilityCost,
  getUtilityCost,
  getUtilityCostes,
  getDtailonMonth,
  getPriceOfAllMonth,
  getYear
}
