import type { Salary } from './Salary'
import type { User } from './User'

type status = 'Present' | 'Absent'
type CheckInOut = {
  id?: number
  email: string
  password: string
  fullName: string
  timeIn: Date
  timeOut: Date | null
  status: status
  totalHour: number
  date: Date
  userId?: number
  user?: User
  salary?: Salary
}

export type { status, CheckInOut }
