//import type { MaterialBillItem } from "./MaterialBillItem";
import type { Material } from './Material'
//import type { MaterialItem } from './MaterialItem'
import type { User } from './User'

type Vender = 'Big C' | 'Makro' | 'Lotus' | '7-11'
type MaterialBill = {
  id: number
  createdDate: Date
  totalPrice?: number
  pricePerUnit?: number
  pay: number
  change: number
  discount?: number
  vender: Vender[]
  //material : Material[]
  //user?: User;
  //materialItems?: MaterialItem[]
  //saved?: boolean;
}
export type { MaterialBill, Vender }
