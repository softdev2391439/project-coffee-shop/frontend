import type { Branch } from './Branch'
import type { User } from './User'

type Status = 'pay' | 'not pay'
type Salary = {
  id?: number
  fullName: string
  role: string
  totalHour: number
  pricePerHour: number
  totalPay: number
  status: Status
  branch: Branch
  payDate: Date
  month: number
  user?: User
  TotalPayment: number
}
export type { Salary, Status }
