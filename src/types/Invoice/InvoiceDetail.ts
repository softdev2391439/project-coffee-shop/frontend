type InvoiceDetail = {
  IND_ID: number
  IND_MATERIAL: number
  IND_AMOUT: number
  IND_PRICE: number
  IND_INVOICE: number
}

export type { InvoiceDetail }
