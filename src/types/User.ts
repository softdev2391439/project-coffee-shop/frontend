import type { Branch } from './Branch'
import type { Role } from './Role'

type User = {
  id?: number
  //Information
  name: string
  gender: string
  height: number
  weight: number
  bloodType: string
  age: number
  birthDate: Date

  //Contact
  phone: string
  email: string
  address: string

  //work
  role: Role
  startDate: Date
  status: string
  salary: number
  branch: Branch

  //Login user Email and Password
  password: string

  //photo
  image: string
}

export type { User }
