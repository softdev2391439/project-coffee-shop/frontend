type UtilityDetail = {
  id?: number
  type: string
  price: number
  date: Date
}
export type { UtilityDetail }
