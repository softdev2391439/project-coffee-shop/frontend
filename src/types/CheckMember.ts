import type { Member } from './Member'

type status = 'Present' | 'Absent'
type CheckMember = {
  id?: number
  email: string
  password: string
  fristName: string
  lastName: string
  timeIn: Date
  timeOut: Date | null
  status: status
  totalHour: number
  date: Date
  memberId?: number
  member?: Member
}

export type { status, CheckMember }
