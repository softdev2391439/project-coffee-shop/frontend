import type { CheckStockItem } from './CheckStockItem'
type CheckStock = {
  id: number
  date: Date
  //userId: number
  //user?: User
  //checkStockItems?: CheckStockItem[]
}
export type { CheckStock }
